<?php


namespace Kowal\KursyWalutNbp\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Store\Model\ScopeInterface;


class UpdateCredit extends Command
{

    const DNI = "ilosc_dni";
    const NAME_OPTION = "option";


    /**
     * @var \Kowal\KursyWalutNbp\Lib\Worker
     */
    protected $worker;
    protected $creditCollectionFactory;
    protected $update;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param \Kowal\KursyWalutNbp\Lib\UpdateCredit $update
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Kowal\KursyWalutNbp\Lib\UpdateCredit              $update,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        parent::__construct();
        $this->update = $update;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        $ilosc_dni = $input->getArgument(self::DNI);
        $option = $input->getOption(self::NAME_OPTION);
        $dni = ($ilosc_dni) ? $ilosc_dni : 1;
        $this->update->run($dni);

        return 1;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_kursywalutnbp:updatecredit");
        $this->setDescription("Aktualizacaj kursu na zamówieniach");
        $this->setDefinition([
            new InputArgument(self::DNI, InputArgument::OPTIONAL, "Ilość dni wstecz"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }

    public function isEnable()
    {
        return $this->scopeConfig->getValue('kowal_kursy_walut_nbp/settings/enable', ScopeInterface::SCOPE_STORE);
    }
}