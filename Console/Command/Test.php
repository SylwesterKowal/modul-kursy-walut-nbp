<?php


namespace Kowal\KursyWalutNbp\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Store\Model\ScopeInterface;


class Test extends Command
{

    const NAME_ARGUMENT = "symbol";
    const NAME_OPTION = "option";


    /**
     * @var \Kowal\KursyWalutNbp\Lib\Worker
     */
    protected $worker;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Test constructor.
     * @param \Kowal\KursyWalutNbp\Lib\Worker $worker
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Kowal\KursyWalutNbp\Lib\Worker                    $worker,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        parent::__construct();
        $this->worker = $worker;
        $this->scopeConfig = $scopeConfig;

    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        $symbol = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);

        if ($this->isEnable()) {
            $this->worker->run();
        } else {
            echo "Moduł jest wyłączony";
        }
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_kursywalutnbp:test");
        $this->setDescription("Sprawdzenie działania Zadania");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Symbol"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }

    public function isEnable()
    {
        return $this->scopeConfig->getValue('kowal_kursy_walut_nbp/settings/enable', ScopeInterface::SCOPE_STORE);
    }
}