<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-13
 * Time: 21:50
 */

namespace Kowal\KursyWalutNbp\Lib;

class MagentoService
{
    public $store_id = 0;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        $this->resourceConnection = $resourceConnection;
    }


    /**
     * @return mixed
     */
    private function _getReadConnection()
    {
        return $this->_getConnection('core_read');
    }


    /**
     * @param string $type
     * @return mixed
     */
    private function _getConnection($type = 'core_read')
    {
        return $this->resourceConnection->getConnection($type);
    }

    /**
     * @param $tableName
     * @return mixed
     */
    private function _getTableName($tableName)
    {
        return $this->resourceConnection->getTableName($tableName);
    }

    /**
     * @param $sku
     * @return mixed
     */
    public function getAllCurrencies()
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT * FROM " . $this->_getTableName('directory_currency_rate') . " WHERE currency_from != currency_to";
        return $connection->fetchAll(
            $sql
        );
    }

    public function setCurrencyRate($rate, $currency_from, $currency_to)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('directory_currency_rate') . " dcr
	                   SET
					   dcr.rate = ?
					   WHERE
					   dcr.currency_from = ? AND dcr.currency_to = ?";


        $connection->query($sql, [$rate, $currency_from, $currency_to]);
    }

    public function setCurrencyAvgRateInOrder($orderId,$kursNbp)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_order') . " so
	                   SET
					   so.kurs_nbp = ?
					   WHERE
					   so.entity_id = ?";


        $connection->query($sql, [$kursNbp, $orderId]);
    }

    public function setCurrencyAvgRateInCredit($CreditId,$kursNbp)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_creditmemo') . " sc
	                   SET
					   sc.kurs_nbp = ?
					   WHERE
					   sc.entity_id = ?";


        $connection->query($sql, [$kursNbp, $CreditId]);
    }

}