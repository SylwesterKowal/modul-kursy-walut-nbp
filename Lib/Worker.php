<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 03/12/2019
 * Time: 21:13
 */

namespace Kowal\KursyWalutNbp\Lib;
use Magento\Store\Model\ScopeInterface;
use MaciejSz\Nbp\Service\CurrencyAverageRatesService;

class Worker
{
    /**
     * @var \Kowal\KursyWalutNbp\Lib\MagentoService
     */
    protected $magentoService;

    /**
     * @var \Kowal\KursyWalutNbp\NbpPhp\NbpRepository
     */
    protected $nbpRepository;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Worker constructor.
     * @param MagentoService $magentoService
     * @param \Kowal\KursyWalutNbp\NbpPhp\NbpRepository $nbpRepository
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Kowal\KursyWalutNbp\Lib\MagentoService $magentoService,
        \Kowal\KursyWalutNbp\NbpPhp\NbpRepository $nbpRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->magentoService = $magentoService;
        $this->nbpRepository = $nbpRepository;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $nameOfDay = date('D', strtotime(date('Y-m-d')));
        if ($nameOfDay == 'Mon') {
            $yesterday = $days_ago = date('Y-m-d', strtotime('-3 days', strtotime(date('Y-m-d'))));
        } else {
            $yesterday = date("Y-m-d", time() - 60 * 60 * 24);
        }


        $currencies = $this->magentoService->getAllCurrencies();
//        var_dump($currencies);

        $currencyAverages =  CurrencyAverageRatesService::new();

        if (is_array($currencies)) {
            foreach ($currencies as $currency) {
                $currency_from = $currency['currency_from'];
                $currency_to = $currency['currency_to'];
                if($currency_from  != 'PLN') {
                    $currencyValueFrom = $currencyAverages->fromEffectiveDay($yesterday)->fromTable('A')->getRate($currency_from);
                    $fromValue = $currencyValueFrom->getValue();
                    $fromCode = $currencyValueFrom->getCurrencyCode();
                }else{
                    $fromValue = 1;
                    $fromCode = 'PLN';
                }
                if($currency_to  != 'PLN') {
                    $currencyValueTo = $currencyAverages->fromEffectiveDay($yesterday)->fromTable('A')->getRate($currency_to);
                    $toValue = $currencyValueTo->getValue();
                    $toCode = $currencyValueTo->getCurrencyCode();
                }else{
                    $toValue = 1;
                    $toCode = 'PLN';
                }
                $percent = $this->getPercent();
                $percent = (is_null($percent)) ? 0 : $percent;
                $rate = $fromValue / $toValue;
                $rate_ = $rate + ($rate * ( $percent  / 100 ));

                $this->magentoService->setCurrencyRate($rate_, $fromCode, $toCode);
                echo "Curency " . $fromCode . '/' . $toCode . ' ' . $fromValue . '/' . $toValue . ' = ' . $rate_ . "\n";
            }
        }
    }

    public function getPercent()
    {
        return $this->scopeConfig->getValue('kowal_kursy_walut_nbp/settings/percent', ScopeInterface::SCOPE_STORE);
    }
}