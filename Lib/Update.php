<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 03/12/2019
 * Time: 21:13
 */

namespace Kowal\KursyWalutNbp\Lib;

use Magento\Store\Model\ScopeInterface;
use MaciejSz\Nbp\Service\CurrencyAverageRatesService;

class Update
{
    /**
     * @var \Kowal\KursyWalutNbp\Lib\MagentoService
     */
    protected $magentoService;

    /**
     * @var \Kowal\KursyWalutNbp\NbpPhp\NbpRepository
     */
    protected $nbpRepository;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    protected $orderCollectionFactory;

    /**
     * Worker constructor.
     * @param MagentoService $magentoService
     * @param \Kowal\KursyWalutNbp\NbpPhp\NbpRepository $nbpRepository
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Kowal\KursyWalutNbp\Lib\MagentoService                    $magentoService,
        \Kowal\KursyWalutNbp\NbpPhp\NbpRepository                  $nbpRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface         $scopeConfig,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
    )
    {
        $this->magentoService = $magentoService;
        $this->nbpRepository = $nbpRepository;
        $this->scopeConfig = $scopeConfig;
        $this->orderCollectionFactory = $orderCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function run($ilosc_dni = 1)
    {

        $fromdate = date('Y-m-d', strtotime('-' . $ilosc_dni . ' days', strtotime(date('Y-m-d'))));
        echo 'Od dnia: ' . $fromdate . PHP_EOL;
        $collection = $this->getOrdersCollection(['created_at' => ['gteq' => $fromdate], 'order_currency_code' => ['neq' => 'PLN']]);

        $currencyAverages = CurrencyAverageRatesService::new();

        foreach ($collection as $order) {
            echo 'order: ' . $order->getEntityId() . " | " . $order->getCreatedAt() . " | " . $order->getOrderCurrencyCode() . PHP_EOL;


            $yesterday = date('Y-m-d', strtotime($order->getCreatedAt()));


            $currencies = $this->magentoService->getAllCurrencies();


            if (is_array($currencies)) {
                foreach ($currencies as $currency) {
                    if ($order->getOrderCurrencyCode() == $currency['currency_from'] && $currency['currency_to'] == 'PLN') {
                        $currency_from = $currency['currency_from'];
                        $currency_to = $currency['currency_to'];

                        if ($currency_from != 'PLN') {
                            $currencyValueFrom = $currencyAverages->fromDayBefore($yesterday)->fromTable('A')->getRate($currency_from);
                            $fromValue = $currencyValueFrom->getValue();
                            $fromCode = $currencyValueFrom->getCurrencyCode();
                            $fromEffectiveDate = $currencyValueFrom->getEffectiveDate()->format('Y-m-d');
                        } else {
                            $fromValue = 1;
                            $fromCode = 'PLN';
                        }
                        if ($currency_to != 'PLN') {
                            $currencyValueTo = $currencyAverages->fromDayBefore($yesterday)->fromTable('A')->getRate($currency_to);
                            $toValue = $currencyValueTo->getValue();
                            $toCode = $currencyValueTo->getCurrencyCode();
                            $toEffectiveDate = $currencyValueTo->getEffectiveDate()->format('Y-m-d');
                        } else {
                            $toValue = 1;
                            $toCode = 'PLN';
                        }

                        $percent = 0;
                        $rate = $fromValue / $toValue;
                        $rate_ = $rate + ($rate * ($percent / 100));

                        echo "Curency " . $fromEffectiveDate . " / " . $fromCode . ' / ' . $toCode . ' ' . $fromValue . ' / ' . $toValue . ' = ' . $rate_ . "\n";
                        $this->magentoService->setCurrencyAvgRateInOrder($order->getEntityId(), $fromValue);
                    }
                }
            }
        }
    }


    public function getOrdersCollection(array $filters = [])
    {

        $collection = $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*');

        foreach ($filters as $field => $condition) {

            $collection->addFieldToFilter($field, $condition);
        }

        return $collection;
    }
}