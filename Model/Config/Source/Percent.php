<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\KursyWalutNbp\Model\Config\Source;

class Percent implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value' => '20', 'label' => __('20%')],
            ['value' => '19%', 'label' => __('19%')],
            ['value' => '18', 'label' => __('18%')],
            ['value' => '17', 'label' => __('17%')],
            ['value' => '16', 'label' => __('16%')],
            ['value' => '15', 'label' => __('15%')],
            ['value' => '14', 'label' => __('14%')],
            ['value' => '13', 'label' => __('13%')],
            ['value' => '12', 'label' => __('12%')],
            ['value' => '11', 'label' => __('11%')],
            ['value' => '10', 'label' => __('10%')],
            ['value' => '9', 'label' => __('9%')],
            ['value' => '8', 'label' => __('8%')],
            ['value' => '7', 'label' => __('7%')],
            ['value' => '6', 'label' => __('6%')],
            ['value' => '5', 'label' => __('5%')],
            ['value' => '4', 'label' => __('4%')],
            ['value' => '3', 'label' => __('3%')],
            ['value' => '2', 'label' => __('2%')],
            ['value' => '1', 'label' => __('1%')],
            ['value' => '0', 'label' => __('0')],
            ['value' => '-1', 'label' => __('-1%')],
            ['value' => '-2', 'label' => __('-2%')],
            ['value' => '-3', 'label' => __('-3%')],
            ['value' => '-4', 'label' => __('-4%')],
            ['value' => '-5', 'label' => __('-5%')],
            ['value' => '-6', 'label' => __('-6%')],
            ['value' => '-7', 'label' => __('-7%')],
            ['value' => '-8', 'label' => __('-8%')],
            ['value' => '-9', 'label' => __('-9%')],
            ['value' => '-10', 'label' => __('-10%')]];
    }

    public function toArray()
    {
        return ['20' => __('20'), '19' => __('19'), '18' => __('18'), '17' => __('17'), '16' => __('16'), '15' => __('15'), '14' => __('14'), '13' => __('13'), '12' => __('12'), '11' => __('11'), '10' => __('10'), '9' => __('9'), '8' => __('8'), '7' => __('7'), '6' => __('6'), '5' => __('5'), '4' => __('4'), '3' => __('3'), '2' => __('2'), '1' => __('1'), '0' => __('0'), '-1' => __('-1'), '-2' => __('-2'), '-3' => __('-3'), '-4' => __('-4'), '-5' => __('-5'), '-6' => __('-6'), '-7' => __('-7'), '-8' => __('-8'), '-9' => __('-9'), '-10' => __('-10')];
    }
}