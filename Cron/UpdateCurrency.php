<?php


namespace Kowal\KursyWalutNbp\Cron;
use Magento\Store\Model\ScopeInterface;

class UpdateCurrency
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var \Kowal\KursyWalutNbp\Lib\Worker
     */
    protected $worker;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * UpdateCurrency constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Kowal\KursyWalutNbp\Lib\Worker $worker
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Kowal\KursyWalutNbp\Lib\Worker $worker
    )
    {
        $this->logger = $logger;
        $this->worker = $worker;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        if ($this->isEnable()) {
            $this->worker->run();
            $this->logger->addInfo("Cronjob UpdateCurrency is executed.");
        }
    }

    public function isEnable()
    {
        return $this->scopeConfig->getValue('kowal_kursy_walut_nbp/settings/enable', ScopeInterface::SCOPE_STORE);
    }
}
