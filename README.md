# Mage2 Module Kowal KursyWalutNbp

    ``kowal/module-kursywalutnbp``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Aktualizacja kursów walut NBP do Magento

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_KursyWalutNbp`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-kursywalutnbp`
 - enable the module by running `php bin/magento module:enable Kowal_KursyWalutNbp`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration




## Specifications

 - Cronjob
	- kowal_kursywalutnbp_updatecurrency


## Attributes


## Usage
Note: currently only average rate is retrieved.
#### Example: getting the average rate from specific day
```php
$nbp = new NbpRepository();
$currencyData = $nbp->getRate('2015-01-02', 'USD');

var_dump($currencyData->avg);
var_dump($currencyData->date);
```

Outputs:
```
double(3.5725)
string(10) "2015-01-02"
```

#### Example: getting the average rate from first date before specified date
This is usefull when you need to retrieve last available currency rate from working day before specified date.
```php
$nbp = new NbpRepository();
$currencyData = $nbp->getRateBefore('2015-01-02', 'USD');

var_dump($currencyData->avg);
var_dump($currencyData->date);
```

Outputs:
```
double(3.5072)
string(10) "2014-12-31"
```
#### Example: using cache

When using cache the amount of HTTP requests to NBP server is minimized.

```php
<?php
use Doctrine\Common\Cache\FilesystemCache;
use MaciejSz\NbpPhp\Service\NbpCache;
use MaciejSz\NbpPhp\NbpRepository;

$cacheBackend = new FilesystemCache(sys_get_temp_dir() . "/nbp-php");
$nbpCache = new NbpCache($cacheBackend);

$nbp = new NbpRepository($nbpCache);
// ...
```
